#Junior software engineer challenge

## The goals
Develop an API and client platform with the following features:

* signup
* login
* update user
* delete user
* list users
* refresh token

The api should have an endpoint for each of the above features, and you should implement on the server and client side the flow to handle password salt, token generation, authentication, expiration and refresh.

### User model [example]
	{
	    "id": "aa43d7ed-a03b-478d-a061-7411ddca000f",
	    "created": "2019-08-26T01:40:01.350529+00:00",
	    "modified": "2019-08-29T05:58:29.022719+00:00",
	    "username": "my_username",
	    "password": "xSubEpwq3r0KGpXfoq05ylY6dDfT/HgBUrqL0JMsXy4=",
	    "name": "my_first_name my_last_name",
	    "email": "my_email@email.com",
	    "salt": null,
	    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2",
	    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8v",
	    "token_expiration": "2019-09-10T20:25:55Z",
	    "refresh_token_expiration": "2019-09-11T20:25:56Z"
	}

## The tools
You can use whatever tools and programing languages you want, as long as at least one of the projects (api/client) you develop uses C# as main language.

## The means
If you want to go the extra-mile, deploy the solution on Amazon AWS, on EC2/ElasticBeanstalk/Lambda and the database on Amazon RDS.

Please let us know if there is any resource you need for you project that is locked on your account and we will make sure we give you access and permissions to all the tools you need.

Log in in on the AWS with the account ID **476638174028** on [http://aws.amazon.com](http://aws.amazon.com)

![Image](login_01.png "aws_login_01")

After you press next, you will be asked for you IAM username and password, witch was provided to you.
**If you don't have login credentials, please reach out.** 

![Image](login_02.png "aws_login_02")


## The expected results
You are expected to deliver a client/server MVP where the signup data sent from the client signup screen gets stored on a database and the password is hashed and salted.

Booth api and client should be able to properly handle all token states and present that information to the user.

The client should be able to present a list of users in the system and allow update and delete actions.


**If you have any doubts, or you can't figure out a part for process, please reach out, we are here to work as a team.**


## Final comments
This challenge is not meant to be done in a specific time window, take your time to architect, develop and even refactor your code

Once you have your project finished, please reach out so we can send you the instructions for you to push your project to a private repository.